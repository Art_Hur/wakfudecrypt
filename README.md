## Synopsis
The aim of this project is to provide an automated way of parsing Wakfu's binary data structures.
It uses Scala macros to automagically expand efficient parsers backed by ByteBuffer.
It contains up-to-date structures for 1.58.5 game version generated via [sherlock](https://gitlab.com/re.wakfu/sherlock).

## Code example
All you need to do to read a binary file is to call ```BinaryDocument.readAll[T]``` or ```BinaryDocument.readToMap[T]```.

```scala
import java.nio.file.Paths

import com.github.wakfudecrypt._
import com.github.wakfudecrypt.types.Item

object Main {
  def main(args: Array[String]) = {
    val data = BinaryDocument.readAll[Item](Paths.get("path/to/Wakfu"))
    data.get.foreach(e => println(e.itemId))
  }
}
```
