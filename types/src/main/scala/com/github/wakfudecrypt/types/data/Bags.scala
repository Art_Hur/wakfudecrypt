package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Bags(
  id: Int,
  capacity: Short,
  bagType: Int,
  validItemCategories: Array[Int]
)

object Bags extends BinaryDataCompanion[Bags] {
  override val dataId = 121
}
