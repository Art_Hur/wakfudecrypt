package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class HavenWorldBuildingEvolution(
  id: Int,
  catalogEntryId: Short,
  fromId: Int,
  toId: Int,
  delay: Long,
  order: Byte
)

object HavenWorldBuildingEvolution extends BinaryDataCompanion[HavenWorldBuildingEvolution] {
  override val dataId = 103
}
