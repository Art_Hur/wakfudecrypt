package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class BoardIeParam(
  id: Int
)

object BoardIeParam extends BinaryDataCompanion[BoardIeParam] {
  override val dataId = 79
}
