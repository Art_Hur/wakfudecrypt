package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class GenericActivableIeParam(
  id: Int,
  visuals: Array[GenericActivableIeParamVisuals],
  _2: GenericActivableIeParam_2
)

@BinaryDecoder
case class GenericActivableIeParam_2(
  _0: Byte,
  _1: Int
)

@BinaryDecoder
case class GenericActivableIeParamVisualsGroupActionsActions(
  actionId: Int,
  actionTypeId: Int,
  criteria: String,
  actionParams: Array[String]
)

@BinaryDecoder
case class GenericActivableIeParamVisualsGroupActions(
  id: Int,
  criteria: String,
  weight: Float,
  actions: Array[GenericActivableIeParamVisualsGroupActionsActions]
)

@BinaryDecoder
case class GenericActivableIeParamVisuals(
  id: Int,
  visualId: Int,
  itemConsumed: Int,
  itemQuantity: Int,
  doConsumeItem: Boolean,
  kamaCost: Int,
  distributionDuration: Int,
  groupActions: Array[GenericActivableIeParamVisualsGroupActions]
)

object GenericActivableIeParam extends BinaryDataCompanion[GenericActivableIeParam] {
  override val dataId = 31
}
