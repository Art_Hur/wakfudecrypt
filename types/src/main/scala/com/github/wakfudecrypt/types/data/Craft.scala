package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Craft(
  craftId: Int,
  bookItemId: Int,
  xpFactor: Float,
  innate: Boolean,
  conceptualCraft: Boolean,
  hiddenCraft: Boolean
)

object Craft extends BinaryDataCompanion[Craft] {
  override val dataId = 23
}
