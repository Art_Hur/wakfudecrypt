package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class MonsterTypeDungeon(
  id: Int,
  familyId: Int,
  dungeonId: Int,
  level: Short
)

object MonsterTypeDungeon extends BinaryDataCompanion[MonsterTypeDungeon] {
  override val dataId = 137
}
