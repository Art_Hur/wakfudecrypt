package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class State(
  id: Int,
  maxLevel: Short,
  endTrigger: Array[Int],
  duration: Array[Int],
  durationInc: Array[Float],
  endsAtEndOfTurn: Boolean,
  isDurationInFullTurns: Boolean,
  inTurnInFight: Boolean,
  isReplacable: Boolean,
  hmiActions: String,
  applyCriterion: String,
  isCumulable: Boolean,
  durationInCasterTurn: Boolean,
  durationInRealTime: Boolean,
  effectIds: Array[Int],
  stateImmunities: Array[Int],
  stateShouldBeSaved: Boolean,
  decursable: Boolean,
  stateType: Byte,
  statePowerType: Byte,
  isReapplyEvenAtMaxLevel: Boolean,
  timelineVisible: Boolean,
  _22: String,
  displayCasterName: Boolean
)

object State extends BinaryDataCompanion[State] {
  override val dataId = 67
}
