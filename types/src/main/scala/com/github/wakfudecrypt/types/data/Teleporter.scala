package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Teleporter(
  teleporterId: Int,
  _1: Array[Teleporter_1]
)

@BinaryDecoder
case class Teleporter_1(
  _0: Int,
  _1: Int,
  _2: Int,
  _3: Int,
  _4: Int,
  _5: Byte,
  _6: String,
  _7: Int,
  _8: Int,
  _9: Short,
  _10: Int,
  _11: Short,
  _12: Short,
  _13: Boolean,
  _14: Boolean,
  _15: Int,
  _16: String,
  _17: Int,
  _18: Int,
  _19: Int
)

object Teleporter extends BinaryDataCompanion[Teleporter] {
  override val dataId = 72
}
