package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class DestructibleIeParam(
  id: Int,
  pdv: Int,
  regenDelay: Int,
  resWater: Int,
  resFire: Int,
  resEarth: Int,
  resWind: Int,
  effectIds: Array[Int]
)

object DestructibleIeParam extends BinaryDataCompanion[DestructibleIeParam] {
  override val dataId = 26
}
