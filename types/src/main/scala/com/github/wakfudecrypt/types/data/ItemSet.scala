package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ItemSet(
  id: Short,
  linkedItemReferenceId: Int,
  itemsId: Array[Int],
  effectIdsByPartCount: Map[Int, Array[Int]]
)

object ItemSet extends BinaryDataCompanion[ItemSet] {
  override val dataId = 36
}
