package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class DecorationIeParam(
  id: Int,
  havreGemTypes: Array[Int]
)

object DecorationIeParam extends BinaryDataCompanion[DecorationIeParam] {
  override val dataId = 25
}
