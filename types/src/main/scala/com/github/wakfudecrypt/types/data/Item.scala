package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Item(
  id: Int,
  metaId: Int,
  itemSetId: Short,
  gfxId: Int,
  femaleGfxId: Int,
  floorGfxId: Int,
  level: Short,
  criteria: Array[String],
  itemTypeId: Int,
  maxStackHeight: Short,
  useCostAP: Byte,
  useCostMP: Byte,
  useCostFP: Byte,
  useRangeMin: Int,
  useRangeMax: Int,
  useTestFreeCell: Boolean,
  useTestNotBorderCell: Boolean,
  useTestLos: Boolean,
  useTestOnlyLine: Boolean,
  itemRarity: Short,
  itemBindType: Byte,
  generationType: String,
  itemProperties: Array[Int],
  itemActionVisual: Byte,
  worldUsageTarget: Byte,
  gemElementType: Byte,
  gemNum: Byte,
  effectIds: Array[Int],
  actions: Array[ItemActions],
  subMetaIds: Array[Int],
  gemSlots: Array[Byte],
  gemSlotType: Array[Byte]
)

@BinaryDecoder
case class ItemActions(
  actionId: Int,
  actionTypeId: Int,
  consumeItemOnAction: Boolean,
  clientOnly: Boolean,
  stopMovement: Boolean,
  hasScript: Boolean,
  criteria: String,
  actionParams: Array[String],
  actionScriptParams: Array[String]
)

object Item extends BinaryDataCompanion[Item] {
  override val dataId = 35
}
