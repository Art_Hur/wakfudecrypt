package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Achievement(
  id: Int,
  categoryId: Int,
  isVisible: Boolean,
  notifyOnPass: Boolean,
  isActive: Boolean,
  criterion: String,
  activationCriterion: String,
  goals: Array[AchievementGoals],
  rewards: Array[AchievementRewards],
  duration: Int,
  cooldown: Int,
  shareable: Boolean,
  repeatable: Boolean,
  needsUserAccept: Boolean,
  recommandedLevel: Int,
  recommandedPlayers: Int,
  followable: Boolean,
  displayOnActivationDelay: Int,
  periodStartTime: Long,
  period: Long,
  autoCompass: Boolean,
  gfxId: Int,
  isMercenary: Boolean,
  mercenaryItemId: Int,
  mercenaryRank: Byte,
  order: Int
)

@BinaryDecoder
case class AchievementRewards(
  id: Int,
  `type`: Int,
  params: Array[Int]
)

@BinaryDecoder
case class AchievementGoalsVlisteners(
  id: Int,
  successCriterion: String,
  variableIds: Array[Int]
)

@BinaryDecoder
case class AchievementGoalsPositions(
  x: Short,
  y: Short,
  z: Short,
  worldId: Short
)

@BinaryDecoder
case class AchievementGoals(
  id: Int,
  feedback: Boolean,
  _2: String,
  positions: Array[AchievementGoalsPositions],
  _4: String,
  vlisteners: Array[AchievementGoalsVlisteners]
)

object Achievement extends BinaryDataCompanion[Achievement] {
  override val dataId = 1
}
