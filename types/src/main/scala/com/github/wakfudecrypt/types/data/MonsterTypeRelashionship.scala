package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class MonsterTypeRelashionship(
  id: Int,
  familyFrom: Int,
  familyTo: Int
)

object MonsterTypeRelashionship extends BinaryDataCompanion[MonsterTypeRelashionship] {
  override val dataId = 49
}
