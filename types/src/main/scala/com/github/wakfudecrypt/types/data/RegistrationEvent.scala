package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class RegistrationEvent(
  _0: Int,
  _1: Int,
  _2: Int,
  _3: Array[Long],
  _4: Int,
  _5: Int
)

object RegistrationEvent extends BinaryDataCompanion[RegistrationEvent] {
  override val dataId = 59
}
