package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class HavenWorldBuildingDefinition(
  _0: Int,
  _1: Short,
  _2: Int,
  _3: Int,
  _4: Byte,
  _5: Byte,
  _6: Int,
  _7: Boolean,
  _8: Array[HavenWorldBuildingDefinition_8],
  _9: Array[HavenWorldBuildingDefinition_9],
  _10: Array[Int],
  _11: Array[HavenWorldBuildingDefinition_11]
)

@BinaryDecoder
case class HavenWorldBuildingDefinition_11(
  _0: Int
)

@BinaryDecoder
case class HavenWorldBuildingDefinition_9(
  _0: Int,
  _1: Int
)

@BinaryDecoder
case class HavenWorldBuildingDefinition_8(
  _0: Int,
  _1: Int,
  _2: Byte,
  _3: Byte,
  _4: Byte
)

object HavenWorldBuildingDefinition extends BinaryDataCompanion[HavenWorldBuildingDefinition] {
  override val dataId = 102
}
