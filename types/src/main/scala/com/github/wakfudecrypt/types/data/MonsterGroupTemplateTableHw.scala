package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class MonsterGroupTemplateTableHw(
  _0: Int,
  _1: Array[Int]
)

object MonsterGroupTemplateTableHw extends BinaryDataCompanion[MonsterGroupTemplateTableHw] {
  override val dataId = 127
}
