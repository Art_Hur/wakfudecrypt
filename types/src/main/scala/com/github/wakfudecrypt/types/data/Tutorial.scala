package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Tutorial(
  id: Int,
  eventIds: Array[TutorialEventIds]
)

@BinaryDecoder
case class TutorialEventIds(
  eventId: Int
)

object Tutorial extends BinaryDataCompanion[Tutorial] {
  override val dataId = 129
}
