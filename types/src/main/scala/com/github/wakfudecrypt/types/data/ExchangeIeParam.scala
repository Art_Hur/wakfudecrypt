package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ExchangeIeParam(
  id: Int,
  apsId: Int,
  visualMruId: Int,
  sortTypeId: Byte,
  exchanges: Array[ExchangeIeParamExchanges],
  _5: ExchangeIeParam_5
)

@BinaryDecoder
case class ExchangeIeParam_5(
  _0: Byte,
  _1: Int
)

@BinaryDecoder
case class ExchangeIeParamExchangesResultings(
  itemId: Int,
  quantity: Short,
  forcedBindType: Byte
)

@BinaryDecoder
case class ExchangeIeParamExchangesConsumables(
  itemId: Int,
  quantity: Short
)

@BinaryDecoder
case class ExchangeIeParamExchanges(
  exchangeId: Int,
  criteria: String,
  consumables: Array[ExchangeIeParamExchangesConsumables],
  consumableKama: Int,
  consumablePvpMoney: Int,
  resultings: Array[ExchangeIeParamExchangesResultings],
  resultingKama: Int
)

object ExchangeIeParam extends BinaryDataCompanion[ExchangeIeParam] {
  override val dataId = 82
}
