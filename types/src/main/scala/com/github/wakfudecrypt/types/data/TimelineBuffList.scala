package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class TimelineBuffList(
  id: Int,
  typeId: Int,
  gfxId: Int,
  forPlayer: Boolean,
  effectIds: Array[Int]
)

object TimelineBuffList extends BinaryDataCompanion[TimelineBuffList] {
  override val dataId = 73
}
