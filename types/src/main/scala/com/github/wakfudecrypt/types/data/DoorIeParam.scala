package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class DoorIeParam(
  id: Int,
  visualId: Int,
  consumeItem: Boolean,
  itemNeeded: Int,
  kamaCost: Int,
  criterion: String
)

object DoorIeParam extends BinaryDataCompanion[DoorIeParam] {
  override val dataId = 118
}
