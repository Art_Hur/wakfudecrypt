package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ArcadeBonus(
  _0: Int,
  _1: Array[Int],
  _2: Array[Int]
)

object ArcadeBonus extends BinaryDataCompanion[ArcadeBonus] {
  override val dataId = 89
}
