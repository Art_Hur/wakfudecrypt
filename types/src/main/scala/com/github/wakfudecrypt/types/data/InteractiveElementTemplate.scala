package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class InteractiveElementTemplate(
  id: Int,
  modelType: Short,
  worldId: Short,
  x: Int,
  y: Int,
  z: Short,
  initialState: Short,
  initiallyVisible: Boolean,
  initiallyUsable: Boolean,
  blockingMovement: Boolean,
  blockingLos: Boolean,
  direction: Byte,
  activationPattern: Short,
  parameter: String,
  templateId: Int,
  properties: Array[Int],
  positionsTrigger: Array[InteractiveElementTemplatePositionsTrigger],
  actions: Map[Short, Int],
  views: Array[Int]
)

@BinaryDecoder
case class InteractiveElementTemplatePositionsTrigger(
  x: Int,
  y: Int,
  z: Short
)

object InteractiveElementTemplate extends BinaryDataCompanion[InteractiveElementTemplate] {
  override val dataId = 128
}
