package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class AudioMarkerIeParam(
  id: Int,
  audioMarkerTypeId: Int,
  isLocalized: Boolean
)

object AudioMarkerIeParam extends BinaryDataCompanion[AudioMarkerIeParam] {
  override val dataId = 5
}
