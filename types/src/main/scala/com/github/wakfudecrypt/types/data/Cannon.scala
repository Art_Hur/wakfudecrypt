package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Cannon(
  cannonId: Int,
  visualId: Int,
  uiGfxId: Int,
  landmarkTravelType: Byte,
  itemId: Int,
  itemQty: Int,
  _6: Array[Cannon_6]
)

@BinaryDecoder
case class Cannon_6_6(
  _0: String,
  _1: Int,
  _2: Int,
  _3: Int
)

@BinaryDecoder
case class Cannon_6(
  _0: Int,
  _1: Int,
  _2: Int,
  _3: Int,
  _4: Int,
  _5: String,
  _6: Cannon_6_6
)

object Cannon extends BinaryDataCompanion[Cannon] {
  override val dataId = 12
}
