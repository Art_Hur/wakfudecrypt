package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class MonsterType(
  id: Int,
  parentId: Int,
  `type`: Byte
)

object MonsterType extends BinaryDataCompanion[MonsterType] {
  override val dataId = 47
}
