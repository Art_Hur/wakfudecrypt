package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class CitizenRank(
  id: Int,
  cap: Int,
  pdcLossFactor: Int,
  translationKey: String,
  color: String,
  rules: Array[Int]
)

object CitizenRank extends BinaryDataCompanion[CitizenRank] {
  override val dataId = 17
}
