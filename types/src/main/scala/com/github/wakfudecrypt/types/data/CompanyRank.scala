package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class CompanyRank(
  _0: Int,
  _1: Short,
  _2: Short,
  _3: Short,
  _4: Short
)

object CompanyRank extends BinaryDataCompanion[CompanyRank] {
  override val dataId = 138
}
