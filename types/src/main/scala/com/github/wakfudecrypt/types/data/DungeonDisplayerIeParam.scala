package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class DungeonDisplayerIeParam(
  id: Int,
  dungeonId: Int
)

object DungeonDisplayerIeParam extends BinaryDataCompanion[DungeonDisplayerIeParam] {
  override val dataId = 95
}
