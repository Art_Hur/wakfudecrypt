package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class AchievementVariable(
  id: Int,
  name: String,
  exportForSteam: Boolean
)

object AchievementVariable extends BinaryDataCompanion[AchievementVariable] {
  override val dataId = 2
}
