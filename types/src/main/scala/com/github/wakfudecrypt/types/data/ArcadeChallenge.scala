package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ArcadeChallenge(
  id: Int
)

object ArcadeChallenge extends BinaryDataCompanion[ArcadeChallenge] {
  override val dataId = 90
}
