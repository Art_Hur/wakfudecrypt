package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ChallengeLootList(
  id: Int,
  entries: Array[ChallengeLootListEntries]
)

@BinaryDecoder
case class ChallengeLootListEntries(
  challengeId: Int,
  criteria: String
)

object ChallengeLootList extends BinaryDataCompanion[ChallengeLootList] {
  override val dataId = 80
}
