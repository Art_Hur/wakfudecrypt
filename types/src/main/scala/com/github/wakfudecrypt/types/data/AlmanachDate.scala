package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class AlmanachDate(
  id: Int,
  date: Long,
  almanachEntryId: Int
)

object AlmanachDate extends BinaryDataCompanion[AlmanachDate] {
  override val dataId = 99
}
