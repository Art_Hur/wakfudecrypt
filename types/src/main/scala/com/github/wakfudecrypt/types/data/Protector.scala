package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Protector(
  protectorId: Int,
  monsterId: Int,
  buffListId: Int,
  buffListIdToBuy: Int,
  scenarioLootListId: Int,
  scenarioLootListIdToBuy: Int,
  scenarioLootListIdChaos: Int,
  scenarioLootListIdEcosystem: Int,
  climateListIdToBuy: Int,
  nationId: Int,
  territory: Int,
  fightStake: Int,
  positionZ: Short,
  craftLearnt: Array[Int],
  secrets: Array[ProtectorSecrets],
  faunaWill: Array[ProtectorFaunaWill],
  floraWill: Array[ProtectorFloraWill]
)

@BinaryDecoder
case class ProtectorFloraWill(
  typeId: Int,
  min: Short,
  max: Short
)

@BinaryDecoder
case class ProtectorFaunaWill(
  typeId: Int,
  min: Short,
  max: Short
)

@BinaryDecoder
case class ProtectorSecrets(
  id: Int,
  achievementGoalId: Int,
  secretGfxId: Int,
  discoveredGfxId: Int
)

object Protector extends BinaryDataCompanion[Protector] {
  override val dataId = 54
}
