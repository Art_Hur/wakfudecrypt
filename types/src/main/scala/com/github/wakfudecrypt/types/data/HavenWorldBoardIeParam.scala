package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class HavenWorldBoardIeParam(
  id: Int,
  visualId: Int,
  havenWorldId: Short,
  miniOriginCellX: Short,
  miniOriginCellY: Short,
  miniOriginCellZ: Short
)

object HavenWorldBoardIeParam extends BinaryDataCompanion[HavenWorldBoardIeParam] {
  override val dataId = 111
}
