package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class AvatarBreed(
  id: Int,
  name: String,
  backgroundAps: Int,
  baseHp: Int,
  baseAp: Int,
  baseMp: Int,
  baseWp: Int,
  baseInit: Int,
  baseFerocity: Int,
  baseFumble: Int,
  baseWisdom: Int,
  baseTackle: Int,
  baseDodge: Int,
  baseProspection: Int,
  timerCountBeforeDeath: Int,
  preferedArea: Int,
  spellElements: Array[Byte],
  characRatios: Array[Float],
  _18: Array[Short]
)

object AvatarBreed extends BinaryDataCompanion[AvatarBreed] {
  override val dataId = 86
}
