package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Boat(
  boatId: Int,
  exitX: Int,
  exitY: Int,
  exitWorldId: Int,
  visualId: Int,
  uiGfxId: Int,
  landmarkTravelType: Byte
)

object Boat extends BinaryDataCompanion[Boat] {
  override val dataId = 8
}
