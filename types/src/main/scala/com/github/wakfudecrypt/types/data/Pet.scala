package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Pet(
  id: Int,
  itemRefId: Int,
  gfxId: Int,
  itemColorRefId: Int,
  itemReskinRefId: Int,
  health: Int,
  minMealInterval: Long,
  maxMealInterval: Long,
  xpByMeal: Byte,
  xpPerLevel: Short,
  levelMax: Short,
  mountType: Byte,
  healthPenalties: Array[PetHealthPenalties],
  healthItems: Array[PetHealthItems],
  mealItems: Array[PetMealItems],
  sleepItems: Array[PetSleepItems],
  equipmentItems: Array[Int],
  colorItems: Array[PetColorItems],
  _18: Array[Pet_18]
)

@BinaryDecoder
case class Pet_18(
  _0: Int,
  _1: Int,
  _2: Int
)

@BinaryDecoder
case class PetColorItems(
  itemId: Int,
  partId: Int,
  colorABGR: Int
)

@BinaryDecoder
case class PetSleepItems(
  itemId: Int,
  duration: Long
)

@BinaryDecoder
case class PetMealItems(
  itemId: Int,
  visible: Boolean
)

@BinaryDecoder
case class PetHealthItems(
  itemId: Int,
  value: Int
)

@BinaryDecoder
case class PetHealthPenalties(
  penaltyType: Byte,
  value: Byte
)

object Pet extends BinaryDataCompanion[Pet] {
  override val dataId = 117
}
