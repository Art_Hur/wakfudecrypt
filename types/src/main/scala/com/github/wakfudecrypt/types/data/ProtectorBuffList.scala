package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ProtectorBuffList(
  buffListId: Int,
  buffLists: Array[Int]
)

object ProtectorBuffList extends BinaryDataCompanion[ProtectorBuffList] {
  override val dataId = 56
}

