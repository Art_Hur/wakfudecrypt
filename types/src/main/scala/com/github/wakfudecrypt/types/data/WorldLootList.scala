package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class WorldLootList(
  _0: Int,
  _1: Double,
  _2: Short,
  _3: Short,
  _4: String,
  _5: Short,
  _6: Array[WorldLootList_6]
)

@BinaryDecoder
case class WorldLootList_6(
  _0: Int,
  _1: Double,
  _2: String,
  _3: Short,
  _4: Short,
  _5: Short,
  _6: Short,
  _7: Short,
  _8: Boolean
)

object WorldLootList extends BinaryDataCompanion[WorldLootList] {
  override val dataId = 75
}
