package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class RecycleMachineIeParam(
  id: Int,
  visualMruId: Int,
  _2: RecycleMachineIeParam_2
)

@BinaryDecoder
case class RecycleMachineIeParam_2(
  _0: Byte,
  _1: Int
)

object RecycleMachineIeParam extends BinaryDataCompanion[RecycleMachineIeParam] {
  override val dataId = 85
}
