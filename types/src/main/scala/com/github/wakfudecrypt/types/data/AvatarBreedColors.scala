package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class AvatarBreedColors(
  id: Int,
  values: Array[AvatarBreedColorsValues]
)

@BinaryDecoder
case class AvatarBreedColorsValuesPupilColors(
  red: Float,
  green: Float,
  blue: Float,
  alpha: Float
)

@BinaryDecoder
case class AvatarBreedColorsValuesHairColors(
  red: Float,
  green: Float,
  blue: Float,
  alpha: Float
)

@BinaryDecoder
case class AvatarBreedColorsValuesSkinColors(
  red: Float,
  green: Float,
  blue: Float,
  alpha: Float
)

@BinaryDecoder
case class AvatarBreedColorsValues(
  sex: Byte,
  defaultSkinIndex: Byte,
  defaultSkinFactor: Byte,
  defaultHairIndex: Byte,
  defaultHairFactor: Byte,
  defaultPupilIndex: Byte,
  skinColors: Array[AvatarBreedColorsValuesSkinColors],
  hairColors: Array[AvatarBreedColorsValuesHairColors],
  pupilColors: Array[AvatarBreedColorsValuesPupilColors]
)

object AvatarBreedColors extends BinaryDataCompanion[AvatarBreedColors] {
  override val dataId = 116
}
