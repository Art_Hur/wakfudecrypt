package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class BackgroundIeParam(
  id: Int,
  visualId: Int,
  backgroundFeedback: Int,
  _3: BackgroundIeParam_3
)

@BinaryDecoder
case class BackgroundIeParam_3(
  _0: Byte,
  _1: Int
)

object BackgroundIeParam extends BinaryDataCompanion[BackgroundIeParam] {
  override val dataId = 7
}
