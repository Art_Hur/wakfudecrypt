package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class AchievementList(
  id: Int,
  elements: Array[AchievementListElements]
)

@BinaryDecoder
case class AchievementListElements(
  achievementId: Int,
  order: Int
)

object AchievementList extends BinaryDataCompanion[AchievementList] {
  override val dataId = 115
}
