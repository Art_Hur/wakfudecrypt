package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ZaapLink(
  id: Int,
  start: Int,
  end: Int,
  cost: Int
)

object ZaapLink extends BinaryDataCompanion[ZaapLink] {
  override val dataId = 77
}
