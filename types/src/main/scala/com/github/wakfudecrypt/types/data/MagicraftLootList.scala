package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class MagicraftLootList(
  id: Int,
  gemType: Byte,
  entries: Array[MagicraftLootListEntries]
)

@BinaryDecoder
case class MagicraftLootListEntries(
  itemId: Int,
  dropRate: Double
)

object MagicraftLootList extends BinaryDataCompanion[MagicraftLootList] {
  override val dataId = 123
}
