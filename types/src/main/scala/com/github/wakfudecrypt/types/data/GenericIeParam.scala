package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class GenericIeParam(
  _0: Int
)

object GenericIeParam extends BinaryDataCompanion[GenericIeParam] {
  override val dataId = 92
}
