package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Drago(
  dragoId: Int,
  exitX: Int,
  exitY: Int,
  visualId: Int,
  uiGfxId: Int,
  dragoCriterion: String,
  landmarkTravelType: Byte,
  _7: Drago_7
)

@BinaryDecoder
case class Drago_7(
  _0: String,
  _1: Int,
  _2: Int,
  _3: Int
)

object Drago extends BinaryDataCompanion[Drago] {
  override val dataId = 28
}
