package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Ability(
  bonusId: Int,
  categoryId: Int,
  max: Int,
  gfxId: Int,
  effectIds: Array[Int]
)

object Ability extends BinaryDataCompanion[Ability] {
  override val dataId = 135
}
