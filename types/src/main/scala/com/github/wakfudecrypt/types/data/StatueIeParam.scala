package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class StatueIeParam(
  _0: Int
)

object StatueIeParam extends BinaryDataCompanion[StatueIeParam] {
  override val dataId = 93
}
