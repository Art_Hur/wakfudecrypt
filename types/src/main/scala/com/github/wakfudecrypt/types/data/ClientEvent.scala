package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ClientEvent(
  id: Int,
  `type`: Int,
  dropRate: Short,
  maxCount: Short,
  criterion: String,
  filters: Array[String],
  activeOnStart: Boolean,
  actionGroups: Array[ClientEventActionGroups]
)

@BinaryDecoder
case class ClientEventActionGroupsActions(
  id: Int,
  `type`: Int,
  params: Array[String]
)

@BinaryDecoder
case class ClientEventActionGroups(
  id: Int,
  dropRate: Short,
  criterion: String,
  actions: Array[ClientEventActionGroupsActions]
)

object ClientEvent extends BinaryDataCompanion[ClientEvent] {
  override val dataId = 18
}
