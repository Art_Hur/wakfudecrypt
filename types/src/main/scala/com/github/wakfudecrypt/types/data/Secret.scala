package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Secret(
  id: Int,
  level: Short,
  itemId: Short
)

object Secret extends BinaryDataCompanion[Secret] {
  override val dataId = 133
}
