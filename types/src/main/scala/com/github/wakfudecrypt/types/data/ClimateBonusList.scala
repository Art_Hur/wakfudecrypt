package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ClimateBonusList(
  buffListId: Int,
  entries: Array[Int]
)

object ClimateBonusList extends BinaryDataCompanion[ClimateBonusList] {
  override val dataId = 21
}
