package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class HavenWorldBuildingCatalog(
  id: Int,
  order: Int,
  buildingType: Int,
  categoryId: Int,
  buyable: Boolean,
  maxQuantity: Short,
  isDecoOnly: Boolean,
  buildingSoundId: Int,
  buildingCondition: Array[HavenWorldBuildingCatalogBuildingCondition]
)

@BinaryDecoder
case class HavenWorldBuildingCatalogBuildingCondition(
  buildingTypeNeeded: Int,
  quantity: Int
)

object HavenWorldBuildingCatalog extends BinaryDataCompanion[HavenWorldBuildingCatalog] {
  override val dataId = 104
}
