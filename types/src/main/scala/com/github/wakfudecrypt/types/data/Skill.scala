package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Skill(
  id: Int,
  `type`: Int,
  scriptId: Int,
  mruGfxId: Int,
  mruKey: String,
  animLinkage: String,
  associatedItemTypes: Array[Int],
  associatedItems: Array[Int],
  maxLevel: Int,
  isInnate: Boolean
)

object Skill extends BinaryDataCompanion[Skill] {
  override val dataId = 64
}
