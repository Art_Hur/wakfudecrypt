package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class KrosmozFigure(
  id: Int,
  year: Int,
  addon: Int,
  season: Int
)

object KrosmozFigure extends BinaryDataCompanion[KrosmozFigure] {
  override val dataId = 113
}
