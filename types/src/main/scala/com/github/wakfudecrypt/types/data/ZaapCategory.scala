package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ZaapCategory(
  _0: Int,
  _1: Int
)

object ZaapCategory extends BinaryDataCompanion[ZaapCategory] {
  override val dataId = 139
}
