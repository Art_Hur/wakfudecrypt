package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class EffectGroup(
  id: Int,
  effectIds: Array[Int]
)

object EffectGroup extends BinaryDataCompanion[EffectGroup] {
  override val dataId = 29
}
