package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class MonsterFightBubble(
  _0: Int,
  _1: Boolean,
  _2: Array[MonsterFightBubble_2],
  _3: Array[MonsterFightBubble_3],
  _4: Array[MonsterFightBubble_4]
)

@BinaryDecoder
case class MonsterFightBubble_4(

)

@BinaryDecoder
case class MonsterFightBubble_3(

)

@BinaryDecoder
case class MonsterFightBubble_2(

)

object MonsterFightBubble extends BinaryDataCompanion[MonsterFightBubble] {
  override val dataId = 44
}
