package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class KrosmozFigureItemLink(
  _0: Int,
  _1: Array[String]
)

object KrosmozFigureItemLink extends BinaryDataCompanion[KrosmozFigureItemLink] {
  override val dataId = 112
}
