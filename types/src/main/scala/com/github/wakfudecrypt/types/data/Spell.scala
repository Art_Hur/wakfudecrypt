package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Spell(
  id: Int,
  scriptId: Int,
  gfxId: Int,
  maxLevel: Short,
  breedId: Short,
  castMaxPerTarget: Short,
  castMaxPerTurn: Float,
  castMaxPerTurnIncr: Float,
  castMinInterval: Short,
  testLineOfSight: Boolean,
  castOnlyLine: Boolean,
  castOnlyInDiag: Boolean,
  testFreeCell: Boolean,
  testNotBorderCell: Boolean,
  testDirectPath: Boolean,
  targetFilter: Int,
  castCriterion: String,
  _17: Short,
  PA_base: Float,
  PA_inc: Float,
  PM_base: Float,
  PM_inc: Float,
  PW_base: Float,
  PW_inc: Float,
  rangeMaxBase: Float,
  rangeMaxInc: Float,
  rangeMinBase: Float,
  rangeMinLevelIncrement: Float,
  maxEffectCap: Short,
  element: Short,
  xpGainPercentage: Short,
  spellType: Short,
  uiPosition: Short,
  learnCriteria: String,
  _34: String,
  passive: Byte,
  useAutomaticDescription: Boolean,
  showInTimeline: Boolean,
  canCastWhenCarrying: Boolean,
  actionOnCriticalMiss: Byte,
  spellCastRangeIsDynamic: Boolean,
  castSpellWillBreakInvisibility: Boolean,
  castOnRandomCell: Boolean,
  tunnelable: Boolean,
  canCastOnCasterCell: Boolean,
  associatedWithItemUse: Boolean,
  properties: Array[Int],
  effectIds: Array[Int],
  baseCastParameters: Map[Byte, SpellBaseCastParameters],
  _49: Map[Short, Spell_49],
  alternativeCasts: Map[String, SpellAlternativeCasts]
)

@BinaryDecoder
case class SpellAlternativeCastsCosts(
  base: Int,
  increment: Float
)

@BinaryDecoder
case class SpellAlternativeCasts(
  _0: Int,
  costs: Map[Int, SpellAlternativeCastsCosts],
  PA_base: Float,
  PA_inc: Float,
  PM_base: Float,
  PM_inc: Float,
  PW_base: Float,
  PW_inc: Float,
  rangeMinBase: Float,
  rangeMinInc: Float,
  rangeMaxBase: Float,
  rangeMaxInc: Float,
  isLosAware: Boolean,
  onlyInLine: Boolean,
  rangeIsDynamic: Boolean,
  _15: Float,
  _16: Float,
  _17: Float,
  _18: Float,
  _19: Boolean,
  _20: Boolean,
  _21: Boolean,
  _22: Boolean
)

@BinaryDecoder
case class Spell_49(
  _0: Short,
  _1: Array[Int]
)

@BinaryDecoder
case class SpellBaseCastParameters(
  base: Int,
  increment: Float
)

object Spell extends BinaryDataCompanion[Spell] {
  override val dataId = 66
}
