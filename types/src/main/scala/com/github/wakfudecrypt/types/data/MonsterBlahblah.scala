package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class MonsterBlahblah(
  _0: Int,
  _1: Int,
  _2: Int,
  _3: Byte,
  _4: Array[MonsterBlahblah_4]
)

@BinaryDecoder
case class MonsterBlahblah_4(
  _0: Int,
  _1: Byte,
  _2: Boolean,
  _3: String
)

object MonsterBlahblah extends BinaryDataCompanion[MonsterBlahblah] {
  override val dataId = 43
}
