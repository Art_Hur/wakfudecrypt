package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class ProtectorBuff(
  buffId: Int,
  gfxId: Int,
  criteria: String,
  origin: Byte,
  effects: Array[Int]
)

object ProtectorBuff extends BinaryDataCompanion[ProtectorBuff] {
  override val dataId = 55
}

