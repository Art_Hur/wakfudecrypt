package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class EquipableDummyIeParam(
  id: Int,
  animName: String,
  sex: Byte
)

object EquipableDummyIeParam extends BinaryDataCompanion[EquipableDummyIeParam] {
  override val dataId = 106
}
