package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class HavenBagModelView(
  id: Int,
  restrictionWorld: Boolean,
  restrictionMarket: Boolean,
  backgroundMapId: Int,
  innate: Boolean,
  _5: Float,
  _6: Float,
  _7: Float
)

object HavenBagModelView extends BinaryDataCompanion[HavenBagModelView] {
  override val dataId = 33
}
