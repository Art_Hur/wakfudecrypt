package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class Ground(
  id: Int,
  resourceFertility: Map[Int, Short],
  resourceTypeFertility: Map[Short, Short]
)

object Ground extends BinaryDataCompanion[Ground] {
  override val dataId = 32
}
