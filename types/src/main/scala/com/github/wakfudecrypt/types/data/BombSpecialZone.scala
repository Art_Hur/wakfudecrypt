package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class BombSpecialZone(
  _0: Int,
  _1: Int,
  _2: Array[BombSpecialZone_2],
  _3: Array[BombSpecialZone_3],
  _4: Array[Int]
)

@BinaryDecoder
case class BombSpecialZone_3(

)

@BinaryDecoder
case class BombSpecialZone_2(

)

object BombSpecialZone extends BinaryDataCompanion[BombSpecialZone] {
  override val dataId = 10
}
