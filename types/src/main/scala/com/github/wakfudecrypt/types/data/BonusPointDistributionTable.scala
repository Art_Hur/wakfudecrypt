package com.github.wakfudecrypt.types.data

import com.github.wakfudecrypt._

@BinaryDecoder
case class BonusPointDistributionTable(
  _0: Short,
  _1: Map[Int, Array[BonusPointDistributionTable_1]]
)

@BinaryDecoder
case class BonusPointDistributionTable_1(
  _0: Short,
  _1: Short,
  _2: Short
)

object BonusPointDistributionTable extends BinaryDataCompanion[BonusPointDistributionTable] {
  override val dataId = 11
}
