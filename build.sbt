
scalaVersion in ThisBuild := "2.12.4"

lazy val buildSettings = Seq(
  organization := "rewakfu",
  version := "0.3-SNAPSHOT",
  scalacOptions ++= compilerOptions,
  resolvers += Resolver.url(
    "scalameta",
    url("http://dl.bintray.com/scalameta/maven"))(Resolver.ivyStylePatterns)
)

lazy val compilerOptions = Seq(
  "-deprecation",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-language:experimental.macros",
  "-unchecked",
  "-Xlint",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard",
  "-Xfuture",
  "-opt:l:inline"
)

lazy val paradisePlugin = Seq(
  addCompilerPlugin("org.scalameta" % "paradise" % "3.0.0-M11" cross CrossVersion.full),
  scalacOptions += "-Xplugin-require:macroparadise"
)

lazy val core =
  project.settings(buildSettings)
    .settings(moduleName := "wakfudecrypt-core")

lazy val macros =
  project.settings(paradisePlugin)
    .settings(buildSettings)
    .settings(
      moduleName := "wakfudecrypt-macros",
      libraryDependencies += "org.scalameta" %% "scalameta" % "1.8.0"
    )

lazy val types =
  project.settings(paradisePlugin)
    .settings(buildSettings)
    .settings(
      moduleName := "wakfudecrypt-types",
      libraryDependencies ++= Seq(
        "com.beachape" %% "enumeratum" % "1.5.13"
      )
    ).dependsOn(core, macros)
