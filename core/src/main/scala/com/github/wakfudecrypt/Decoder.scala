package com.github.wakfudecrypt

trait Decoder[T] {
  def decode(b: RandomizedBuffer): T
}
