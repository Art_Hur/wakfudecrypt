package com.github.wakfudecrypt

trait Randomizer {
  def mul: Int

  def add: Int

  def getPosition: Int

  @inline final def inc(): Unit =
    seed = (seed + (mul * getPosition + add).toByte).toByte

  var seed: Byte = (mul ^ add).toByte
}
