package com.github.wakfudecrypt

import java.nio.file.{FileSystems, Files, Path, Paths}
import java.nio.{ByteBuffer, ByteOrder}

object BinaryDocument {

  private case class Entry(id: Long, position: Int, size: Int, seed: Byte)

  private case class Header(version: Int, entries: Seq[Entry])

  /** Returns a map of all entries by their id
    *
    * @param path path to Wakfu main directory
    */
  def readToMap[A: Decoder : HasBinaryDataId](path: Path): Option[Map[Long, A]] =
    readAllEntries(path).map(_.toMap)

  /** Returns a lazy view of all entries in the file
    *
    * @param path path to Wakfu main directory
    */
  def readAll[A: Decoder : HasBinaryDataId](path: Path): Option[Traversable[A]] =
    readAllEntries(path).map(_.map(_._2))

  private def slurpDataFile(path: Path, id: Int): Option[Array[Byte]] = {
    if (Files.exists(path)) {
      val fs = FileSystems.newFileSystem(path, null)
      val bytes = Files.readAllBytes(fs.getPath(s"$id.bin"))
      fs.close()
      Some(bytes)
    } else None
  }

  private def readAllEntries[A: Decoder: HasBinaryDataId](path: Path): Option[Traversable[(Long, A)]] = {
    val id = implicitly[HasBinaryDataId[A]].id
    slurpDataFile(Paths.get(path.toString, s"game/contents/bdata/$id.jar"), id)
      .map(readAllEntries(_, id))
  }

  private def readHeader(bb: ByteBuffer, id: Int): Header = {
    /*
      ignoring data structures at beginning of the file,
      since they're just for indexing - to quickly navigate
      through binary files in game
     */
    val version = bb.getInt + 756423
    val rb = new RandomizedBuffer(bb, id, version)
    val entryCount = rb.getInt
    val entries =
      Seq.fill(entryCount) {
        Entry(
          rb.getLong,
          rb.getInt,
          rb.getInt,
          rb.getByte
        )
      }
    val indexCount = rb.getByte
    for (_ <- 0 until indexCount.toInt) {
      val unique = rb.getByte != 0
      rb.getUTF8
      val count = rb.getInt
      for (_ <- 0 until count) {
        rb.getLong
        if (unique)
          rb.getInt
        else {
          val len = rb.getInt
          for (_ <- 0 until len)
            rb.getInt
        }
      }
    }
    Header(version, entries)
  }

  private def readAllEntries[A: Decoder](bytes: Array[Byte], id: Int): Traversable[(Long, A)] = {
    val bb = ByteBuffer.wrap(bytes)
    bb.order(ByteOrder.LITTLE_ENDIAN)

    val header = readHeader(bb, id)
    val slice = bb.slice()
    slice.order(bb.order)
    val db = new RandomizedBuffer(slice, id, header.version)
    for (entry <- header.entries.view) yield {
      db.setPosition(entry.position)
      db.seed = entry.seed
      (entry.id, implicitly[Decoder[A]].decode(db))
    }
  }
}
